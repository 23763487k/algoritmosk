# Algoritmo que dado un número entero (este numero no podrá ser menor o igual que 0),  
# determine el número de cifras que tiene. Por ejemplo, si introduzco un 253, 
# me devuelva un 3.
def calcularCifrasIterador(num):
    i = 0
    for digito in str(num):
        i +=1
    return i

def calcularCifrasLen(num):
    return len(str(num))

numero = 0
def calcularCifrasNum(num):
    contador = 0
    while(num > 0):
        print (num)
        num = num//10
        contador +=1
    return contador

numero = 0
while(numero <=0):
    numero = int(input('Ingrese un número mayor que cero : '))
print("la cantidad de cifras por iteracion es ",calcularCifrasIterador(numero))
print("la cantidad de cifras por calculo de largo es  ",calcularCifrasLen(numero))
print("la cantidad de cifras por calculo division entera  ",calcularCifrasNum(numero))