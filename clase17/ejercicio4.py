# Los empleados de una fábrica trabajan en dos turnos, Diurno y Nocturno. 
# Se desea calcular el jornal diario de acuerdo a con las siguientes reglas: 
#   La tarifa de las horas diurnas es de $3000.
#   La tarifa de las horas nocturnas es de $4500.
#   En caso de ser festivo, la tarifa se incrementa en un 10% en caso de turno diurno y en un 15% para el nocturno. 
#   Escribe una función en llamada jornal(…) que tome como parámetros 
#       el nº de horas, el turno y el tipo de día (“Festivo”, “Laborable”) y nos devuelva el sueldo a cobrar. 

# Escribe también un algoritmo principal que pida 
#   el nombre del trabajador, el día de la semana, turno y nº de horas trabajadas, 
# nos escriba el sueldo a cobrar usando la función anterior.
#   Ten en cuenta, que en la función nos pide el tipo de día pero en el algoritmo 
# le pedimos al día  es decir, que debemos saber si el dia que introduce el usuario es festivo o no.

HDiurna = 3000
HNocturna = 4500
DiasFestivos = [1, 21]

def calcularJornal(dia, turno, CantHoras):
    sueldo = 0
    if dia in DiasFestivos:
        if turno == "d":
            sueldo = int(HDiurna * CantHoras * 1.1)
        else:
            sueldo = int(HNocturna * CantHoras * 1.15)
    else:
        if turno == "d":
            sueldo = HDiurna * CantHoras
        else:
            sueldo = HNocturna * CantHoras
    return sueldo

def CalculoTrabajador():
    nombre = input("Nombre: ")
    while True:                     # Ingreso Dia
        dia = int(input("Dia del mes trabajado : "))
        if dia >= 1 and dia <= 31:
            break
    while True:                     # Ingreso Turno
        turno = input("Diurno (d) o Nocturno (n): ")
        if turno == "d" or turno == "n":
           break
    CantHoras = int(input("Cantidad de horas trabajadas: "))

    sueldo = calcularJornal(dia, turno, CantHoras)

    if turno == "d":
        turno = "Diurno"
    else:
        turno = "Nocturno"

    print("\nEstimado", nombre)
    print("El día", dia, "ha trabajado durante el turno", turno)
    print("Ha ganado", sueldo, "por la cantidad de", CantHoras, "horas trabajadas.\n")
    
    return

CalculoTrabajador()