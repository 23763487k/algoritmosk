# Escribe una función en pseudocódigo que devuelva el término N 
# (siendo N un número entero positivo) de la serie de Fibonacci, 
# esta sigue la siguiente serie: 1, 1, 2, 3, 5, 8, 13, 21… y así sucesivamente. 
# Date cuenta, que para obtener un numero, suma los dos números anteriores. 
# Por ejemplo, si introducimos un 3, la función nos devuelve el 2.
def fibonacci(ultimo):
    fibo1 = 0
    fibo2 = 1
    sumatoria = 0
    if (ultimo not in range(1,2)):
        for x in range(ultimo-2):
            print(sumatoria)
            sumatoria = fibo1+fibo2
            fibo1 = fibo2
            fibo2=sumatoria
    else:
        sumatoria = ultimo
    return sumatoria


numero = int(input('Ingrese la cantidad de fiboacci a calcular : '))
resultado = fibonacci(numero)
print ("El fibonacci posicion ",numero, "es igual a",resultado )