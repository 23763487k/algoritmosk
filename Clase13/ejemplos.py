#           STRINGS
# miString = "hola mundo"
#multilinea
# miString = '''
#                 qui dolorem ipsum, quia dolor sit amet
#                 consectetur adipisci velit, sed quia non
#                 numquam eius modi tempora incidunt, ut labore et dolore
#                 magnam aliquam quaerat voluptatem
#
#        '''
#por posicion
# myString = "Ciao Mundo!"
# print(myString[-6])
#recooriendo
# myString = "Ciao Mundo!"
# for letra in myString:
#     print(letra)
#largo de string
# myString = "Ciao Mundo!"
# print(len(myString))
#si contiene o no
# myString = "Ciao Mundo!"
# print("Ciao" not in myString)
#rango/substring
# myString = "Ciao Mundo!"
# print(myString[2:8])
#slice (cortar string)
# myString = "Ciao Mundo!"
# cortar = slice(8)
# print(myString[cortar])
#mayusculas y minusculas
# myString = "Ciao Mundo!"
# print(myString.upper())
# print(myString.lower())
#reemplazar
# myString = "Ciao Mundo!"
# print(myString.replace("Ciao","Buongorno"))
# myString = "Ciao, Mundo, buongiorno, principessa!"
# print(myString.split(','))
#concatenacion
# myString = "Ciao Mundo!"
# myOtherString = " Cómo están??"
# print(myString+myOtherString)