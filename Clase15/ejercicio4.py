# 1.- Debe crear un algoritmo que sea capaz de resolver un determinante de 3x3, 
# considerando un array de dos dimensiones. Proporcione un menú que permita que el 
# usuario elija si desea llenar manualmente el determinante o con números al azar. 
# Debe mostrar por pantalla el determinante y la solución. Considere la regla de 
# Sarrus 

import random
matriz = []
for x in range(3):
    linea = []
    for y in range (3):
        numero = random.randint(1,5)
        linea.append(numero)
    matriz.append(linea)
    print(linea)
resultado = 0
resultado += (matriz[0][0]*matriz[1][1]*matriz[2][2])
resultado += (matriz[0][1]*matriz[1][2]*matriz[2][0])
resultado += (matriz[1][0]*matriz[2][1]*matriz[0][2])
resultado -= (matriz[2][0]*matriz[1][1]*matriz[0][2])
resultado -= (matriz[2][1]*matriz[1][2]*matriz[0][0])
resultado -= (matriz[1][0]*matriz[0][1]*matriz[2][2])

print("Segun el señor Sarrus, el resultado de la Matriz es: ",resultado)