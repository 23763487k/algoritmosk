"""Debe crear una función, que al solicitar el RUT, antes del guion, sea capaz de calcular el dígito verificador a través de una función. 
Base su método en el explicado en la siguiente pagina web : https://validarutchile.cl/calcular-digito-verificador.php"""

rut = input("Ingrese el rut sin puntos, guion, ni digito verificador : ")
rut = "".join(reversed(rut))

# print("\n",rut,"\n")

multiplicador = 2
suma = 0

for digito in rut:
    suma += int(digito)*multiplicador
    multiplicador += 1
    if (multiplicador > 7):
        multiplicador = 2
        
calculo = suma // 11                    # El resultado obtenido lo dividimos por 11, para luego obter el Resto de esa división.
calculo = calculo * 11                  # Tomamos el resultado sin decimales y sin aproximación. Y lo multimplicamos por 11

# print (suma)
# print (calculo)

verif= int(11) - (suma - calculo)       # Posteriormente, al resultado le restamos lo obtenido anteriormente. Y el resultado se lo restamos a 11.

if (verif == 10):                       # Si es 10 el dígito será la letra K.
    verif = "K"
if (verif == 11):                       # Si nos da el número 11, el dígito verificador será 0
    verif = 0

# print (verif)

print ("\nSu dígito verificador es : - ",verif)