print("Calculadora de Hipotenusa ...")

a = int(input("Medida Cateto A: "))
b = int(input("Medida Cateto B: "))
c2 = a*a+b*b

if(a<=0):
    print("Cateto A no puede ser 0")
else:
    if(b<=0):
        print("Cateto B no puede ser 0")
    else:
        print("la hipotenusa es de : ",c2)