# 4.- Escriba un algoritmo que almacene los números del loto 
#       (6 números del 1 al 39) y luego una haga sorteos aleatorios hasta que se obtenga el ganador. 
# El retorno de la función debe ser la cantidad de veces que se hizo el sorteo.

import random
def UserEleccion():              # El usuario elije 5 numeros sin repetir
    eleXion = 0
    while eleXion < 5:
        UserNum = int(input("Elija un Numero del 1 al 39, sin repetir : "))
        if UserNum >= 1 and UserNum <= 39 and not UserNum in UserLoto:
            UserLoto.append(UserNum)
            eleXion += 1
        else:
            print("Recuerde, un numero del 1 al 39")
    UserLoto.sort()
    print("\nUsted eligio los siguientes numeros : ",UserLoto,"\n")
    return 

def Sorteo():                     # Funcion de Sorteo, 5 numeros al azar sin repetir
    sorteo = 0
    while sorteo < 5:
        RandNum = random.randint(1,39)
        if RandNum >= 1 and RandNum <= 39 and not RandNum in SorteoLoto:        # No repite numeros
            SorteoLoto.append(RandNum)
            sorteo += 1
        SorteoLoto.sort()
    return

def Suerte():                 # Intentos hasta coincidir elegido y sorteos
    NumSorteos = 0
    UserEleccion()
    while True:
        NumSorteos += 1
        Sorteo()
        if UserLoto == SorteoLoto:
            print ("Ha coincidido !!!")
            break
        else:
            del SorteoLoto[:]           # Borrar el contenido de la lista, para un nuevo sorteo
    print ("\nSe han hecho la siguiente cantidad de Sorteos : ", NumSorteos,"\n")
    return

UserLoto = []                           # Lista de numeros del usuario
SorteoLoto = []                         # Listado de numeros del sorteo

Suerte()

