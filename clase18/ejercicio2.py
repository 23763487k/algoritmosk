"""2.- Crea un función “ConvertirEspaciado”, que reciba como parámetro un texto y devuelve una cadena con un espacio 
            adicional tras cada letra. 
Por ejemplo, “Hola, tú” --- devolverá --- “H o l a , t ú “. 

 Crea un programa principal donde se use dicha función."""

def ConvertirEspaciado(palabra):
    nuevaPalabra = ''
    for letra in palabra:
        if (letra != " "):
            nuevaPalabra += letra + " "
    return nuevaPalabra

ingreso = input("palabra a espaciar : ")
print ("La palabra espaciada es : ",ConvertirEspaciado(ingreso))