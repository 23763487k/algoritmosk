# Imagine una batalla pokemon en la que ambos pokemones
# comienzan con mil puntos.
# por cada iteración cada pokemon irá perdiendo una cantidad ramdom(10)
# de puntaje.
# Debe señalar cual es el pokemon que sobrevive a la batalla, 
# la cantidad de ataques hechos y el puntaje final de vida
import random
pokemon1 = input('ingrese el pokemon 1 : ')
pokemon2 = input('ingrese el pokemon 2 : ')
energiaPoke1 = 1000
energiaPoke2 = 1000
i =0
while (energiaPoke1 > 0 and energiaPoke2 > 0):
    ataque1 = random.randint(1,10)
    ataque2 = random.randint(1,10)
    energiaPoke1 -= ataque1
    energiaPoke2 -= ataque2
    i+=1
    print("Golpe n ",i, pokemon1 ,"(",energiaPoke1,") -- ",pokemon2,"(",energiaPoke2,")")
if(energiaPoke1 > energiaPoke2):
    print(pokemon1, " ha ganado la batalla, acertando ",i," golpes y quedó con ",energiaPoke1, " de vida")
else: 
    if(energiaPoke1 < energiaPoke2):
        print(pokemon2, " ha ganado la batalla, acertando ",i," golpes y quedó con ",energiaPoke2, " de vida")
    else:
        print("Ha sido un empate")