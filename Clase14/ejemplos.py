########################## METODOS HOMOLOGOS DE STRINGS
"""por posicion
print(miLista[5])"""

"""recorriendo
for elemento in miLista:
    print(elemento)"""
    
"""largo de lista
print(len(miLista))"""

"""si contiene o no
print(9 in miLista)"""

"""rango/sublista
print(miLista[2:8])"""

"""slice (cortar lista)
cortar = slice(8)
print(miLista[cortar])"""

"""concatenacion
myOtherlista = ["colocolo","everton","wanderers"]
print(miLista + myOtherlista)"""
##########################################################
#       comparaciones
# print( miLista == myOtherlista)

#       tipo de dato
# print(type(miLista))

#       insertar
# miLista.insert(3,"U.Española")
# print(miLista)

#       agregar al final
# miLista.append("U.Española")
# print(miLista)

#       extend
# miLista.extend(myOtherlista)
# print(miLista)

#       remove
# miLista.remove("colocolo")
# print(miLista)

#       remover por indice / ultima posición
# miLista.pop()
# print(miLista)

#       Borrar por posicion
# del miLista[3]
# print(miLista)

#       del miLista
# print(miLista)
miLista = ["wanderers","everton","colocolo","huachipato","cobreloa"]
myOtherlista = ["wanderers","everton","SanLorenzo","Chacaritas"]
i = 0
while (i< len(miLista)):
    miLista.pop(0)
    # del miLista[0]
print(miLista)

